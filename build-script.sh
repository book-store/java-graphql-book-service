#!/bin/bash

set -e

# run docker build
function dockerBuild {
    echo "build image wildfly-book-service"
    docker build --tag=wildfly-book-service .
}

# check if the image exesist then remove it
# then build
function build {

    name="wildfly-book-service"

    image_id=`docker images | awk '{ print $1,$3 }' | grep "$name" | awk '{ print $2 }'`

    if [ -z "$image_id" ]
    then
        echo "no image"
        dockerBuild
        exit 1
    fi

    docker rmi -f "$image_id"
    dockerBuild

    echo "done"
}

# run command docker run
function run {
    docker run -d --name book-service --link book-postgres:postgres -p 8080:8080 -p 9990:9990 wildfly-book-service
}

# check if docker is installed
function isDockerInstalled {
    if ! docker_loc="$(type -p "docker")" || [ -z "$docker_loc" ];
    then
        echo "need to install docker"
        exit 1
    fi
}

isDockerInstalled

command=$1

if [ "$command" == "build" ]
then
    echo "run docker build"
    build
else
    if [ "$command" == "run" ]
    then
        echo "run docker run"
        run
    else
        echo "the only command you can give is \"run\" or \"build\" "
    fi
fi

