package com.hongduc.service;

import com.hongduc.graphql.Schema;
import com.hongduc.util.ModelUtil;
import graphql.ExecutionResult;
import graphql.GraphQL;
import graphql.GraphQLError;
import java.util.List;
import java.util.Map;
import javax.inject.Inject;
import javax.ws.rs.BadRequestException;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 *
 * @author duc
 */
@Path("")
public class GraphQLService {

    GraphQL graphQL;

    @Inject
    public GraphQLService(Schema schema) {
        this.graphQL = new GraphQL(schema.getSchema());
    }

    public GraphQLService() {
    }

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public String query(String query) {
        ExecutionResult result = this.graphQL.execute(query);
        List<GraphQLError> errors = result.getErrors();
        Map<String, Object> data = (Map<String, Object>) result.getData();
        if (!errors.isEmpty()) {
            StringBuilder error = new StringBuilder();
            errors.forEach(err -> {
                error.append("Message: ")
                        .append(err.getMessage())
                        .append(", Type: ")
                        .append(err.getErrorType().name())
                        .append("\n");
            });
            throw new BadRequestException(error.toString(),
                    Response.status(Response.Status.BAD_REQUEST).entity(error.toString()).build());
        } else {
            return ModelUtil.convertToJsonString(data);
        }
    }
}
