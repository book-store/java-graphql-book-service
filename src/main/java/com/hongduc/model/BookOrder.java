package com.hongduc.model;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author duc
 */
@Entity
@Table(name = "book_order")
public class BookOrder implements Serializable {

    @Id
    @Column(name = "book_id")
    private Integer bookId;

    @Id
    @Column(name = "order_id")
    private Integer orderId;

    public Integer getBookId() {
        return bookId;
    }

    public void setBookId(Integer bookId) {
        this.bookId = bookId;
    }

    public Integer getOrderId() {
        return orderId;
    }

    public void setOrderId(Integer orderId) {
        this.orderId = orderId;
    }

}
