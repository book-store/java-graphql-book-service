package com.hongduc.model;

import graphql.annotations.GraphQLField;
import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author duc
 */
@Entity
@Table(name = "category_map")
public class CategoryMap implements Serializable {

    @Id
    @Column(name = "book_id")
    private Integer bookId;

    @Id
    @Column(name = "category_id")
    private Integer categoryId;

    public Integer getBookId() {
        return bookId;
    }

    public void setBookId(Integer bookId) {
        this.bookId = bookId;
    }

    public Integer getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Integer categoryId) {
        this.categoryId = categoryId;
    }

}
