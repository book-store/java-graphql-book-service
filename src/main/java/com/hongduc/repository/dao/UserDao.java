package com.hongduc.repository.dao;

import com.hongduc.model.UserAccount;
import com.hongduc.model.UserAccount_;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaUpdate;
import javax.persistence.criteria.Root;

/**
 *
 * @author duc
 */
@Stateless
public class UserDao {

    @PersistenceContext(unitName = "PersistenceUnit")
    private EntityManager entityManager;

    @Inject
    private Logger log;

    public boolean save(UserAccount user) {
        try {
            this.entityManager.persist(user);
            return true;
        } catch (Exception ex) {
            this.log.severe(ex.getMessage());
            return false;
        }
    }

    public int updateUser(UserAccount user) {
        try {
            CriteriaBuilder builder = this.entityManager.getCriteriaBuilder();

            CriteriaUpdate update = builder.createCriteriaUpdate(UserAccount.class);
            Root<UserAccount> updateRoot = update.from(UserAccount.class);
            update.where(builder.equal(updateRoot.get(UserAccount_.user_name), user.getUser_name()));
            if (user.getFirst_name() != null || !"null".equals(user.getFirst_name()) || !"".equals(user.getFirst_name())) {
                update.set(updateRoot.get(UserAccount_.first_name), user.getFirst_name());
            }
            if (user.getLast_name() != null || !"null".equals(user.getLast_name()) || !"".equals(user.getLast_name())) {
                update.set(updateRoot.get(UserAccount_.last_name), user.getLast_name());
            }
            if (user.getAddress() != null || !"null".equals(user.getAddress()) || !"".equals(user.getAddress())) {
                update.set(updateRoot.get(UserAccount_.address), user.getAddress());
            }
            if (user.getCity() != null || !"null".equals(user.getCity()) || !"".equals(user.getCity())) {
                update.set(updateRoot.get(UserAccount_.city), user.getCity());
            }
            if (user.getDistrict() != null || !"null".equals(user.getDistrict()) || !"".equals(user.getDistrict())) {
                update.set(updateRoot.get(UserAccount_.district), user.getDistrict());
            }
            if (user.getEmail() != null || !"null".equals(user.getEmail()) || !"".equals(user.getEmail())) {
                update.set(updateRoot.get(UserAccount_.email), user.getEmail());
            }
            if (user.getPhone() != null || !"null".equals(user.getPhone()) || !"".equals(user.getPhone())) {
                update.set(updateRoot.get(UserAccount_.phone), user.getPhone());
            }

            Query q = this.entityManager.createQuery(update);
            int num_row = q.executeUpdate();
            return num_row;
        } catch (Exception ex) {
            this.log.log(Level.SEVERE, ex, () -> ex.getMessage());
            return 0;
        }

    }
}
