package com.hongduc.repository;

import com.hongduc.model.Publisher;
import java.util.List;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.persistence.EntityManager;

/**
 *
 * @author duc
 */
@ApplicationScoped
public class PublisherRepo {

    @Inject
    private EntityManager em;

    public List<Publisher> getAllPublisher() {
        return em.createQuery("Select pub From Publisher pub", Publisher.class).getResultList();
    }

    public Publisher getPublisherById(int id) {
        return em.createQuery("Select p From Publisher p Where p.id = :id", Publisher.class)
                .setParameter("id", id)
                .getSingleResult();
    }
}
