package com.hongduc.repository;

import com.hongduc.repository.dao.UserDao;
import com.hongduc.model.UserAccount;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;

/**
 *
 * @author duc
 */
public class UserRepo {

    @Inject
    private EntityManager em;

    @Inject
    Logger log;

    @EJB
    private UserDao userDao;

    /**
     * Lay user theo username va password, tra ve empty userAccount neu khong co
     *
     * @param username
     * @param password
     * @return
     */
    public UserAccount getUser(String username, String password) {
        try {
            return this.em.createQuery("Select user From UserAccount user Where user.user_name = :username And user.password = :password", UserAccount.class)
                    .setParameter("username", username)
                    .setParameter("password", password)
                    .getSingleResult();
        } catch (NoResultException ex) {
            log.warning("Invalid login");
            return new UserAccount();
        }
    }

    /**
     * kiem tra username co ton tai chua tra ve false neu khong, true neu co
     *
     * @param username
     * @return
     */
    public boolean checkIfUserNameExists(String username) {
        try {
            this.em.createQuery("Select user From UserAccount user Where user.user_name = :username", UserAccount.class)
                    .setParameter("username", username)
                    .getSingleResult();
            return false;
        } catch (NoResultException ex) {
            return true;
        }
    }

    public boolean createUser(String username, String password, String email) {
        UserAccount user = new UserAccount();
        user.setUser_name(username);
        user.setPassword(password);
        user.setEmail(email);
        return this.userDao.save(user);

    }

    public UserAccount updateUser(UserAccount user) {
        int row_num = this.userDao.updateUser(user);
        if (row_num == 0) {
            return new UserAccount();
        }
        return user;
    }

}
