package com.hongduc.repository;

import com.hongduc.model.Author;
import java.util.List;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.persistence.EntityManager;

/**
 *
 * @author duc
 */
@ApplicationScoped
public class AuthorRepo {

    @Inject
    private EntityManager em;

    public List<Author> getAllAuthor() {
        return em.createQuery("Select au From Author au", Author.class).getResultList();
    }

    public Author getAuthorById(int id) {
        return em.createQuery("Select a From Author a Where a.id = :id", Author.class)
                .setParameter("id", id)
                .getSingleResult();
    }
}
