package com.hongduc.repository;

import com.hongduc.model.Book;
import java.util.List;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.persistence.EntityManager;

/**
 *
 * @author duc
 */
@ApplicationScoped
public class BookRepo {

    @Inject
    private EntityManager em; // duoc inject tu com.hongduc.util.Resources class

    public List<Book> getAllBook() {
        return em.createQuery("Select b From Book b", Book.class).getResultList();
    }

    public Book getBookById(int id) {
        return em.createQuery("Select b From Book b Where b.id = :id", Book.class)
                .setParameter("id", id)
                .getSingleResult();
    }

    public List<Book> getByAuthor(int id) {
        return em.createQuery("Select b from Book b where b.author_id = :id", Book.class)
                .setParameter("id", id)
                .getResultList();
    }

    public List<Book> getByPublisher(int id) {
        return em.createQuery("Select b from Book b where b.publisher_id = :id", Book.class)
                .setParameter("id", id)
                .getResultList();
    }

    public List<Book> getByCategory(int id) {
        return em.createQuery("Select b from Book b where b.id in (Select cm.bookId From CategoryMap cm Where cm.categoryId = :id)", Book.class)
                .setParameter("id", id)
                .getResultList();
    }
}
