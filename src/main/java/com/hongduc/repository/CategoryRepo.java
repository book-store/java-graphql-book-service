package com.hongduc.repository;

import com.hongduc.model.Category;
import java.util.List;
import javax.inject.Inject;
import javax.persistence.EntityManager;

/**
 *
 * @author duc
 */
public class CategoryRepo {

    @Inject
    EntityManager em;

    public List<Category> getAllCategory() {
        return this.em.createQuery("Select c from Category c", Category.class).getResultList();
    }

    public Category getById(int id) {
        return this.em.createQuery("Select c from Category c where c.id = :id", Category.class)
                .setParameter("id", id)
                .getSingleResult();
    }

    public List<Category> getByBookId(int id) {
        return this.em.createQuery("Select c From Category c Where c.id in (Select cm.categoryId From CategoryMap cm Where cm.bookId = :id)", Category.class)
                .setParameter("id", id)
                .getResultList();
    }
}
