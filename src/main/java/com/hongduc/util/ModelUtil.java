package com.hongduc.util;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.util.Map;
import org.apache.log4j.Logger;

/**
 *
 * @author duc
 */
public class ModelUtil {

    private static final ObjectMapper MAPPER = new ObjectMapper();
    private static final Logger LOGGER = Logger.getLogger(ModelUtil.class);

    public static Map<String, Object> convertToMap(Object object) {
        return MAPPER.convertValue(object, Map.class);
    }

    public static String convertToJsonString(Object object) {
        try {
            return MAPPER.writeValueAsString(object);
        } catch (JsonProcessingException e) {
            LOGGER.error(e.getMessage(), e);
            return "";
        }

    }
}
