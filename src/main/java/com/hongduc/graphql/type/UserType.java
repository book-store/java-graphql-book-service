package com.hongduc.graphql.type;

import com.hongduc.repository.UserRepo;
import graphql.Scalars;
import static graphql.schema.GraphQLFieldDefinition.newFieldDefinition;
import graphql.schema.GraphQLObjectType;
import static graphql.schema.GraphQLObjectType.newObject;
import javax.annotation.PostConstruct;
import javax.inject.Inject;

/**
 *
 * @author duc
 */
public class UserType {

    private GraphQLObjectType userType;

    @Inject
    private UserRepo userRepo;

    @PostConstruct
    private void init() {
        this.userType = newObject()
                .name("UserType")
                .field(newFieldDefinition()
                        .name("user_name")
                        .type(Scalars.GraphQLString))
                .field(newFieldDefinition()
                        .name("first_name")
                        .type(Scalars.GraphQLString))
                .field(newFieldDefinition()
                        .name("last_name")
                        .type(Scalars.GraphQLString))
                .field(newFieldDefinition()
                        .name("address")
                        .type(Scalars.GraphQLString))
                .field(newFieldDefinition()
                        .name("phone")
                        .type(Scalars.GraphQLString))
                .field(newFieldDefinition()
                        .name("email")
                        .type(Scalars.GraphQLString))
                .field(newFieldDefinition()
                        .name("city")
                        .type(Scalars.GraphQLString))
                .field(newFieldDefinition()
                        .name("district")
                        .type(Scalars.GraphQLString))
                .build();
    }

    public GraphQLObjectType getUserType() {
        return userType;
    }

}
