package com.hongduc.graphql.type;

import graphql.Scalars;
import static graphql.schema.GraphQLInputObjectField.newInputObjectField;
import graphql.schema.GraphQLInputObjectType;
import static graphql.schema.GraphQLInputObjectType.newInputObject;
import graphql.schema.GraphQLNonNull;
import javax.annotation.PostConstruct;

/**
 *
 * @author duc
 */
public class UserInputType {

    private GraphQLInputObjectType userInputType;

    @PostConstruct
    private void init() {
        this.userInputType = newInputObject()
                .name("UserInputType")
                .field(newInputObjectField()
                        .name("user_name")
                        .type(new GraphQLNonNull(Scalars.GraphQLString)))
                .field(newInputObjectField()
                        .name("first_name")
                        .type(Scalars.GraphQLString))
                .field(newInputObjectField()
                        .name("last_name")
                        .type(Scalars.GraphQLString))
                .field(newInputObjectField()
                        .name("address")
                        .type(Scalars.GraphQLString))
                .field(newInputObjectField()
                        .name("phone")
                        .type(Scalars.GraphQLString))
                .field(newInputObjectField()
                        .name("email")
                        .type(Scalars.GraphQLString))
                .field(newInputObjectField()
                        .name("city")
                        .type(Scalars.GraphQLString))
                .field(newInputObjectField()
                        .name("district")
                        .type(Scalars.GraphQLString))
                .build();
    }

    public GraphQLInputObjectType getUserInputType() {
        return userInputType;
    }

    public void setUserInputType(GraphQLInputObjectType userInputType) {
        this.userInputType = userInputType;
    }

}
