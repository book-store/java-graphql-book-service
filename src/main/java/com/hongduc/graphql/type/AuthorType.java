/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hongduc.graphql.type;

import com.hongduc.model.Book;
import com.hongduc.repository.BookRepo;
import com.hongduc.util.ModelUtil;
import graphql.Scalars;
import graphql.schema.DataFetchingEnvironment;
import static graphql.schema.GraphQLFieldDefinition.newFieldDefinition;
import graphql.schema.GraphQLList;
import graphql.schema.GraphQLObjectType;
import static graphql.schema.GraphQLObjectType.newObject;
import graphql.schema.GraphQLTypeReference;
import java.util.List;
import java.util.Map;
import static java.util.stream.Collectors.toList;
import javax.annotation.PostConstruct;
import javax.inject.Inject;

/**
 *
 * @author duc
 */
public class AuthorType {

    private GraphQLObjectType authorType;

    @Inject
    private BookRepo bookRepo;

    @PostConstruct
    private void init() {
        this.authorType = newObject()
                .name("AuthorType")
                .description("tag gia sach")
                .field(newFieldDefinition()
                        .name("id")
                        .description("id cua tac gia")
                        .type(Scalars.GraphQLInt))
                .field(newFieldDefinition()
                        .name("name")
                        .description("ten cua tac gia")
                        .type(Scalars.GraphQLString))
                .field(newFieldDefinition()
                        .name("books")
                        .type(new GraphQLList(new GraphQLTypeReference("BookType")))
                        .description("nhung cuon sach ma tac gia viet")
                        .dataFetcher(this::fetchBooks))
                .build();
    }

    public GraphQLObjectType getAuthorType() {
        return authorType;
    }

    private List<Object> fetchBooks(DataFetchingEnvironment environment) {
        Map<String, Object> author = (Map<String, Object>) environment.getSource();
        List<Book> books = this.bookRepo.getByAuthor((int) author.get("id"));
        return books.stream().map(ModelUtil::convertToMap).collect(toList());
    }

}
