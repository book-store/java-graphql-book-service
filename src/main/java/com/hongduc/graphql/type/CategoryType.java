package com.hongduc.graphql.type;

import com.hongduc.model.Book;
import com.hongduc.repository.BookRepo;
import com.hongduc.util.ModelUtil;
import graphql.Scalars;
import graphql.schema.DataFetchingEnvironment;
import static graphql.schema.GraphQLFieldDefinition.newFieldDefinition;
import graphql.schema.GraphQLList;
import graphql.schema.GraphQLObjectType;
import static graphql.schema.GraphQLObjectType.newObject;
import graphql.schema.GraphQLTypeReference;
import java.util.List;
import java.util.Map;
import static java.util.stream.Collectors.toList;
import javax.annotation.PostConstruct;
import javax.inject.Inject;

/**
 *
 * @author duc
 */
public class CategoryType {

    private GraphQLObjectType categoryType;

    @Inject
    private BookRepo bookRepo;

    @PostConstruct
    private void init() {
        this.categoryType = newObject()
                .name("CategoryType")
                .field(newFieldDefinition()
                        .name("id")
                        .description("id cua category")
                        .type(Scalars.GraphQLInt))
                .field(newFieldDefinition()
                        .name("name")
                        .description("name cua category")
                        .type(Scalars.GraphQLString))
                .field(newFieldDefinition()
                        .name("books")
                        .description("nhung cuon sach thuoc ve danh muc nay")
                        .type(new GraphQLList(new GraphQLTypeReference("BookType")))
                        .dataFetcher(this::fetchBooks))
                .build();
    }

    private List<Object> fetchBooks(DataFetchingEnvironment environment) {
        Map<String, Object> category = (Map<String, Object>) environment.getSource();
        List<Book> books = this.bookRepo.getByCategory((int) category.get("id"));
        return books.stream().map(ModelUtil::convertToMap).collect(toList());
    }

    public GraphQLObjectType getCategoryType() {
        return categoryType;
    }

}
