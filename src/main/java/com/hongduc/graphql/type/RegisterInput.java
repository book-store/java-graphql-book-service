package com.hongduc.graphql.type;

import graphql.Scalars;
import static graphql.schema.GraphQLInputObjectField.newInputObjectField;
import graphql.schema.GraphQLInputObjectType;
import static graphql.schema.GraphQLInputObjectType.newInputObject;
import graphql.schema.GraphQLNonNull;
import javax.annotation.PostConstruct;

/**
 *
 * @author duc
 */
public class RegisterInput {

    private GraphQLInputObjectType registerInput;

    @PostConstruct
    private void init() {
        this.registerInput = newInputObject()
                .name("RegisterInput")
                .field(newInputObjectField()
                        .name("userName")
                        .type(new GraphQLNonNull(Scalars.GraphQLString)))
                .field(newInputObjectField()
                        .name("email")
                        .type(new GraphQLNonNull(Scalars.GraphQLString)))
                .field(newInputObjectField()
                        .name("password")
                        .type(new GraphQLNonNull(Scalars.GraphQLString)))
                .build();
    }

    public GraphQLInputObjectType getRegisterInput() {
        return registerInput;
    }

    public void setRegisterInput(GraphQLInputObjectType registerInput) {
        this.registerInput = registerInput;
    }

}
