package com.hongduc.graphql.type;

import com.hongduc.model.Book;
import com.hongduc.repository.BookRepo;
import com.hongduc.util.ModelUtil;
import graphql.Scalars;
import graphql.schema.DataFetchingEnvironment;
import static graphql.schema.GraphQLFieldDefinition.newFieldDefinition;
import graphql.schema.GraphQLList;
import graphql.schema.GraphQLObjectType;
import static graphql.schema.GraphQLObjectType.newObject;
import graphql.schema.GraphQLTypeReference;
import java.util.List;
import java.util.Map;
import static java.util.stream.Collectors.toList;
import javax.annotation.PostConstruct;
import javax.inject.Inject;

/**
 *
 * @author duc
 */
public class PublisherType {

    private GraphQLObjectType publisherType;

    @Inject
    private BookRepo bookRepo;

    @PostConstruct
    private void init() {
        this.publisherType = newObject()
                .name("PublisherType")
                .field(newFieldDefinition()
                        .name("id")
                        .description("id cua nha xuat ban")
                        .type(Scalars.GraphQLInt))
                .field(newFieldDefinition()
                        .name("name")
                        .description("ten cua nha xuat ban")
                        .type(Scalars.GraphQLString))
                .field(newFieldDefinition()
                        .name("books")
                        .description("cac sach cua nxb nay")
                        .type(new GraphQLList(new GraphQLTypeReference("BookType")))
                        .dataFetcher(this::fetchBook))
                .build();
    }

    public GraphQLObjectType getPublisherType() {
        return publisherType;
    }

    private List<Object> fetchBook(DataFetchingEnvironment environment) {
        Map<String, Object> source = (Map<String, Object>) environment.getSource();
        List<Book> books = this.bookRepo.getByPublisher((int) source.get("id"));
        return books.stream().map(ModelUtil::convertToMap).collect(toList());
    }

}
