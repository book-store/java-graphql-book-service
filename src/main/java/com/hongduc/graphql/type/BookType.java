package com.hongduc.graphql.type;

import com.hongduc.model.Author;
import com.hongduc.model.Category;
import com.hongduc.model.Publisher;
import com.hongduc.repository.AuthorRepo;
import com.hongduc.repository.CategoryRepo;
import com.hongduc.repository.PublisherRepo;
import com.hongduc.util.ModelUtil;
import graphql.Scalars;
import graphql.schema.DataFetchingEnvironment;
import static graphql.schema.GraphQLFieldDefinition.newFieldDefinition;
import graphql.schema.GraphQLList;
import graphql.schema.GraphQLObjectType;
import static graphql.schema.GraphQLObjectType.newObject;
import graphql.schema.GraphQLTypeReference;
import java.util.List;
import java.util.Map;
import static java.util.stream.Collectors.toList;
import javax.annotation.PostConstruct;
import javax.inject.Inject;

/**
 *
 * @author duc
 */
public class BookType {

    private GraphQLObjectType bookType;

    @Inject
    private AuthorRepo authorRepo;

    @Inject
    private PublisherRepo publisherRepo;

    @Inject
    private CategoryRepo categoryRepo;

    @PostConstruct
    private void init() {
        this.bookType = newObject()
                .name("BookType")
                .description("quyen sach")
                .field(newFieldDefinition()
                        .name("id")
                        .type(Scalars.GraphQLInt)
                        .description("id cua quyen sach"))
                .field(newFieldDefinition()
                        .name("isbn")
                        .type(Scalars.GraphQLString)
                        .description("ma isbn cua quyen sach"))
                .field(newFieldDefinition()
                        .name("title")
                        .description("title cua quyen sach")
                        .type(Scalars.GraphQLString))
                .field(newFieldDefinition()
                        .name("publish_year")
                        .description("ngay xuat ban cua sach")
                        .type(Scalars.GraphQLString))
                .field(newFieldDefinition()
                        .name("about")
                        .description("mieu ta cua sach")
                        .type(Scalars.GraphQLString))
                .field(newFieldDefinition()
                        .name("price")
                        .description("gia quyen sach")
                        .type(Scalars.GraphQLFloat))
                .field(newFieldDefinition()
                        .name("image_url")
                        .description("url toi hinh anh")
                        .type(Scalars.GraphQLString))
                .field(newFieldDefinition()
                        .name("author")
                        .description("tac gia cua quyen sach")
                        .type(new GraphQLTypeReference("AuthorType"))
                        .dataFetcher(this::authorFetcher))
                .field(newFieldDefinition()
                        .name("publisher")
                        .description("nha xuat ban cua sach")
                        .type(new GraphQLTypeReference("PublisherType"))
                        .dataFetcher(this::publisherFetcher))
                .field(newFieldDefinition()
                        .name("language")
                        .description("ngon ngu cua sach")
                        .type(Scalars.GraphQLString))
                .field(newFieldDefinition()
                        .name("categories")
                        .description("sach thuoc ve danh muc")
                        .type(new GraphQLList(new GraphQLTypeReference("CategoryType")))
                        .dataFetcher(this::categoriesFetcher))
                .build();
    }

    public GraphQLObjectType getBookType() {
        return bookType;
    }

    private Object authorFetcher(DataFetchingEnvironment environment) {
        Map<String, Object> book = (Map<String, Object>) environment.getSource();
        Author result = this.authorRepo.getAuthorById((int) book.get("author_id"));
        return ModelUtil.convertToMap(result);
    }

    private Object publisherFetcher(DataFetchingEnvironment environment) {
        Map<String, Object> book = (Map<String, Object>) environment.getSource();
        Publisher result = this.publisherRepo.getPublisherById((int) book.get("publisher_id"));
        return ModelUtil.convertToMap(result);
    }

    private List<Object> categoriesFetcher(DataFetchingEnvironment environment) {
        Map<String, Object> book = (Map<String, Object>) environment.getSource();
        List<Category> categories = this.categoryRepo.getByBookId((int) book.get("id"));
        return categories.stream().map(ModelUtil::convertToMap).collect(toList());
    }

}
