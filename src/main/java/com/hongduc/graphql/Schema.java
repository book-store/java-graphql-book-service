package com.hongduc.graphql;

import com.hongduc.graphql.type.AuthorType;
import com.hongduc.graphql.type.BookType;
import com.hongduc.graphql.type.CategoryType;
import com.hongduc.graphql.type.PublisherType;
import com.hongduc.graphql.type.RegisterInput;
import com.hongduc.graphql.type.UserInputType;
import com.hongduc.graphql.type.UserType;
import com.hongduc.model.Author;
import com.hongduc.model.Book;
import com.hongduc.model.Category;
import com.hongduc.model.Publisher;
import com.hongduc.model.UserAccount;
import com.hongduc.repository.AuthorRepo;
import com.hongduc.repository.BookRepo;
import com.hongduc.repository.CategoryRepo;
import com.hongduc.repository.PublisherRepo;
import com.hongduc.repository.UserRepo;
import com.hongduc.util.ModelUtil;
import graphql.Scalars;
import graphql.schema.DataFetchingEnvironment;
import static graphql.schema.GraphQLArgument.newArgument;
import static graphql.schema.GraphQLFieldDefinition.newFieldDefinition;
import graphql.schema.GraphQLList;
import graphql.schema.GraphQLNonNull;
import static graphql.schema.GraphQLObjectType.newObject;
import graphql.schema.GraphQLSchema;
import java.util.List;
import java.util.Map;
import static java.util.stream.Collectors.toList;
import javax.annotation.PostConstruct;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

/**
 *
 * @author user
 */
@ApplicationScoped
public class Schema {

    private GraphQLSchema schema;

    @Inject
    private BookType bookType;

    @Inject
    private AuthorType authorType;

    @Inject
    private PublisherType publisherType;

    @Inject
    private CategoryType categoryType;

    @Inject
    private RegisterInput registerInput;

    @Inject
    private UserInputType userInputType;

    @Inject
    private UserType userType;

    @Inject
    private BookRepo bookRepo;

    @Inject
    private AuthorRepo authorRepo;

    @Inject
    private PublisherRepo publisherRepo;

    @Inject
    private CategoryRepo categoryRepo;

    @Inject
    private UserRepo userRepo;

    @PostConstruct
    public void init() {
        this.schema = GraphQLSchema.newSchema()
                .query(newObject()
                        .name("QueryType")
                        .field(newFieldDefinition()
                                .name("books")
                                .type(new GraphQLList(bookType.getBookType()))
                                .dataFetcher(this::fetchBooks))
                        .field(newFieldDefinition()
                                .name("book")
                                .type(bookType.getBookType())
                                .argument(newArgument()
                                        .name("id")
                                        .type(new GraphQLNonNull(Scalars.GraphQLInt))
                                        .description("id cua book"))
                                .dataFetcher(this::fetchBook))
                        .field(newFieldDefinition()
                                .name("author")
                                .type(authorType.getAuthorType())
                                .argument(newArgument()
                                        .name("id")
                                        .type(new GraphQLNonNull(Scalars.GraphQLInt))
                                        .description("id cua author"))
                                .dataFetcher(this::fetchAuthor))
                        .field(newFieldDefinition()
                                .name("authors")
                                .description("lay het tat ca tac gia")
                                .type(new GraphQLList(authorType.getAuthorType()))
                                .dataFetcher(this::fetchAuthors))
                        .field(newFieldDefinition()
                                .name("publisher")
                                .type(publisherType.getPublisherType())
                                .argument(newArgument()
                                        .name("id")
                                        .type(new GraphQLNonNull(Scalars.GraphQLInt))
                                        .description("id cua publisher"))
                                .dataFetcher(this::fetchPublisher))
                        .field(newFieldDefinition()
                                .name("publishers")
                                .description("lay het publisher")
                                .type(new GraphQLList(publisherType.getPublisherType()))
                                .dataFetcher(this::fetchPublishers))
                        .field(newFieldDefinition()
                                .name("category")
                                .type(this.categoryType.getCategoryType())
                                .argument(newArgument()
                                        .name("id")
                                        .type(new GraphQLNonNull(Scalars.GraphQLInt)))
                                .dataFetcher(this::fetchCategory))
                        .field(newFieldDefinition()
                                .name("categories")
                                .type(new GraphQLList(this.categoryType.getCategoryType()))
                                .dataFetcher(this::fetchCategories))
                        .field(newFieldDefinition()
                                .name("user")
                                .type(this.userType.getUserType())
                                .argument(newArgument()
                                        .name("username")
                                        .type(new GraphQLNonNull(Scalars.GraphQLString)))
                                .argument(newArgument()
                                        .name("password")
                                        .type(new GraphQLNonNull(Scalars.GraphQLString)))
                                .dataFetcher(this::fetchUser))
                )
                .mutation(newObject()
                        .name("MutationType")
                        .field(newFieldDefinition()
                                .name("register")
                                .type(Scalars.GraphQLString)
                                .argument(newArgument()
                                        .name("registerInfo")
                                        .type(this.registerInput.getRegisterInput()))
                                .dataFetcher(this::register))
                        .field(newFieldDefinition()
                                .name("updateUser")
                                .type(this.userType.getUserType())
                                .argument(newArgument()
                                        .name("userInput")
                                        .type(this.userInputType.getUserInputType()))
                                .dataFetcher(this::updateUser)))
                .build();
    }

    public GraphQLSchema getSchema() {
        return schema;
    }

    private Object fetchBook(DataFetchingEnvironment environment) {
        int id = environment.getArgument("id");
        Book book = this.bookRepo.getBookById(id);
        return ModelUtil.convertToMap(book);
    }

    private List<Object> fetchBooks(DataFetchingEnvironment environment) {
        List<Book> books = this.bookRepo.getAllBook();
        return books.stream().map(ModelUtil::convertToMap).collect(toList());
    }

    private Object fetchAuthor(DataFetchingEnvironment environment) {
        int id = environment.getArgument("id");
        Author author = this.authorRepo.getAuthorById(id);
        return ModelUtil.convertToMap(author);
    }

    private List<Object> fetchAuthors(DataFetchingEnvironment environment) {
        List<Author> authors = this.authorRepo.getAllAuthor();
        return authors.stream().map(ModelUtil::convertToMap).collect(toList());
    }

    private Object fetchPublisher(DataFetchingEnvironment environment) {
        int id = environment.getArgument("id");
        Publisher publisher = this.publisherRepo.getPublisherById(id);
        return ModelUtil.convertToMap(publisher);
    }

    private List<Object> fetchPublishers(DataFetchingEnvironment environment) {
        List<Publisher> publishers = this.publisherRepo.getAllPublisher();
        return publishers.stream().map(ModelUtil::convertToMap).collect(toList());
    }

    private Object fetchCategory(DataFetchingEnvironment environment) {
        int id = environment.getArgument("id");
        Category category = this.categoryRepo.getById(id);
        return ModelUtil.convertToMap(category);
    }

    private List<Object> fetchCategories(DataFetchingEnvironment environment) {
        return this.categoryRepo.getAllCategory().stream()
                .map(ModelUtil::convertToMap)
                .collect(toList());
    }

    private Object fetchUser(DataFetchingEnvironment environment) {
        String username = environment.getArgument("username");
        String password = environment.getArgument("password");
        UserAccount user = this.userRepo.getUser(username, password);
        return ModelUtil.convertToMap(user);
    }

    private String register(DataFetchingEnvironment environment) {
        Map<String, Object> registerInfo = environment.getArgument("registerInfo");
        String userName = (String) registerInfo.get("userName");
        String password = (String) registerInfo.get("password");
        String email = (String) registerInfo.get("email");
        if (!this.userRepo.checkIfUserNameExists(userName)) {
            return "username da ton tai";
        }

        if (!this.userRepo.createUser(userName, password, email)) {
            return "loi xay ra khi tao user";
        }

        return "";
    }

    private Object updateUser(DataFetchingEnvironment environment) {
        Map<String, Object> userInput = environment.getArgument("userInput");
        UserAccount user = new UserAccount();
        user.setUser_name((String) userInput.get("user_name"));
        user.setFirst_name((String) userInput.get("first_name"));
        user.setLast_name((String) userInput.get("last_name"));
        user.setAddress((String) userInput.get("address"));
        user.setPhone((String) userInput.get("phone"));
        user.setEmail((String) userInput.get("email"));
        user.setCity((String) userInput.get("city"));
        user.setDistrict((String) userInput.get("district"));
        user = this.userRepo.updateUser(user);
        return ModelUtil.convertToMap(user);
    }

}
