
    create table administrator (userName varchar(20) not null,address varchar(255),email varchar(255),password varchar(255),primary key (userName));

    create table author (id  serial not null,name varchar(255) not null,primary key (id));

    create table book (id  serial not null,about varchar(300),image_url varchar(255),isbn varchar(255) not null,price decimal(14,2) not null,publish_year integer,title varchar(255) not null,author_id int4,publisher_id int4,primary key (id));

    create table book_order (order_id int4 not null,book_id int4 not null);

    create table category (id  serial not null,name varchar(50) not null,primary key (id));

    create table category_map (book_id int4 not null,category_id int4 not null);

    create table customer_order (id  serial not null,delivery_date timestamp,order_date timestamp not null,order_status_id int4,user_name varchar(20),primary key (id));

    create table customer_order_status (id  serial not null,decription varchar(255) not null,primary key (id));

    create table publisher (id  serial not null,name varchar(255) not null,primary key (id));

    create table user_account (user_name varchar(20) not null,address varchar(255),city varchar(255),district varchar(255),email varchar(255) not null,first_name varchar(255),is_login boolean not null,last_login timestamp not null,last_name varchar(255),password varchar(255) not null,phone varchar(20),primary key (user_name));

    create index unique_index_category_name on category (name);

    alter table category add constraint UK_foei036ov74bv692o5lh5oi66 unique (name);

    alter table book add constraint FK5gbo4o7yxefxivwuqjichc67t foreign key (author_id) references author;

    alter table book add constraint FKrb2njmkvio5mhe42empuaiphu foreign key (publisher_id) references publisher;

    alter table book_order add constraint FK5gld4ehof4adffdn3gy7s77ns foreign key (book_id) references book;

    alter table book_order add constraint FK7msdhwa4vlctps3d5vwajo29g foreign key (order_id) references customer_order;

    alter table category_map add constraint FKpou4ggvncy16nf3p0ja02aghg foreign key (category_id) references category;

    alter table category_map add constraint FK7iv3pf5skt81cf87a2e60dfwq foreign key (book_id) references book;

    alter table category_map add constraint primary_key_const primary key (book_id,category_id);

    alter table customer_order add constraint FKaetg3g0oltbod3qi11nofq5pp foreign key (order_status_id) references customer_order_status;

    alter table customer_order add constraint FK44mmf9kw316yuwcpgt0wse6c3 foreign key (user_name) references user_account;
